#!/bin/sh

# Profile file. Runs on login.

export PATH="$(du $HOME/.local/share/scripts/ | cut -f2 | tr '\n' ':')$PATH"
export EDITOR="nvim"
export SUDO_EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="brave"
export READER="zathura"
export PRIVATE_BROWSER="brave --incognito"
export SHELL="/bin/zsh"

# Start graphical server if i3 not already running.
if [ "$(tty)" = "/dev/tty1" ]; then
        pgrep -x i3 || exec startx
fi
