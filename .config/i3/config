# #---Basic Definitions---# #
# Needed for i3-gaps
for_window [class="^.*"] border pixel 2
gaps inner 15
gaps outer 0
set $term --no-startup-id $TERMINAL
set $mod Mod4
set $shutdown sudo -A shutdown -h now
set $reboot sudo -A reboot
set $netrefresh --no-startup-id sudo -A systemctl restart NetworkManager
set $hibernate sudo -A systemctl suspend

# #---Starting External Scripts---# #
# Setting the background and colorscheme:
exec --no-startup-id cp ~/Pictures/wallpapers/Landscapes/$(ls ~/Pictures/wallpapers/Landscapes/ | sort -R | head -1 ) ~/.config/wall.png
#exec --no-startup-id ~/.fehbg
exec --no-startup-id wal -c
exec --no-startup-id wal -i ~/.config/wall.png >/dev/null
# Starts dunst for notifications:
exec --no-startup-id dunst
# Composite manager:
exec --no-startup-id xcompmgr
# Unclutter makes the mouse invisible after a brief period
exec --no-startup-id unclutter
# Enables touchpad gestures
exec --no-startup-id libinput-gestures-setup start
# Network manager applet
#exec --no-startup-id nm-applet

# Set Xresources colors:
set_from_resource $darkblack	color0  #000000
set_from_resource $black	color8  #000000
set_from_resource $darkred	color1  #000000
set_from_resource $red		color9  #000000
set_from_resource $darkgreen	color2  #000000
set_from_resource $green	color10 #000000
set_from_resource $darkyellow	color3  #000000
set_from_resource $yellow	color11 #000000
set_from_resource $darkblue	color4  #000000
set_from_resource $blue		color12 #000000
set_from_resource $darkmagenta	color5  #000000
set_from_resource $magenta	color13 #000000
set_from_resource $darkcyan	color6  #000000
set_from_resource $cyan		color14 #000000
set_from_resource $darkwhite	color7  #000000
set_from_resource $white	color15 #000000
set $transparent		#00000000

#                       BORDER		BACKGROUND	TEXT		INDICATOR   CHILD_BORDER
client.focused		$red		$red		$magenta	$darkmagenta		$darkblue
client.unfocused	$transparent	$blue		$white		$darkblue		$darkblack
client.focused_inactive	$transparent	$blue		$white		$darkblue		$darkblack
client.urgent		$darkred	$darkred	$black		$darkred		$darkred
client.background $black

bar {
	font pango:mono 12
	colors {
			background $darkblack
			statusline $darkwhite
			separator $cyan
			focused_workspace  $blue $darkblue $darkblack
        		active_workspace   $blue $blue $darkwhite
      			inactive_workspace $darkblack $darkblack $white
        		urgent_workspace   $darkblack $darkblack $white
		}
	status_command i3blocks
	position top
	mode dock
	modifier None
}

# #---Basic Bindings---# #
bindsym $mod+Return 		exec $term

bindsym $mod+Shift+space 	floating toggle
bindsym $mod+space		focus mode_toggle

bindsym $mod+Escape		workspace prev
bindsym $mod+Shift+Escape 	exec --no-startup-id prompt "Exit i3?" "i3-msg exit"

# bindsym $mod+BackSpace
bindsym $mod+Shift+BackSpace	exec --no-startup-id prompt "Reboot computer?" "$reboot"

bindsym $mod+comma		exec --no-startup-id dmenuunicode
##bindsym $mod+asciitilde

# #---Letter Key Bindings---# #
bindsym $mod+q			[con_id="__focused__" instance="^(?!dropdowncalc|tmuxdd).*$"] kill
bindsym $mod+Shift+q		[con_id="__focused__" instance="^(?!dropdowncalc|tmuxdd).*$"] kill

bindsym $mod+w		exec --no-startup-id $BROWSER
bindsym $mod+Shift+w		exec --no-startup-id $PRIVATE_BROWSER

bindsym $mod+r 			exec $term -e ranger

bindsym $mod+t			split toggle
bindsym $mod+Shift+t		gaps inner current set 15; gaps outer current set 15

bindsym $mod+Shift+y		exec --no-startup-id i3resize left

bindsym $mod+Shift+u		exec --no-startup-id i3resize down

bindsym $mod+i 			exec $term -e htop
bindsym $mod+Shift+i		exec --no-startup-id i3resize up

bindsym $mod+o			sticky toggle
bindsym $mod+Shift+o		exec --no-startup-id i3resize right

bindsym $mod+Shift+a		exec $term -e pulsemixer

bindsym $mod+s			gaps inner current plus 5
bindsym $mod+Shift+s		gaps inner current minus 5

bindsym $mod+Control+s exec --no-startup-id xrandr --output HDMI1 --mode 2560x1440 --output eDP1 --off && feh --bg-scale ~/.config/wall.png
bindsym $mod+Control+Shift+s exec --no-startup-id xrandr --output eDP1 --auto --output HDMI1 --off && feh --bg-scale ~/.config/wall.png

bindsym $mod+d			exec --no-startup-id dmenu_run -fn 'Noto-14.0'
bindsym $mod+Shift+d		gaps inner current set 0; gaps outer current set 0

set $freeze Distraction-free mode (mod+shift+esc to exit)
mode "$freeze" { bindsym $mod+Shift+Escape mode "default"
}

bindsym $mod+f			fullscreen toggle
bindsym $mod+Shift+f		mode "$freeze"

bindsym $mod+g			workspace prev

bindsym $mod+h			focus left
bindsym $mod+Shift+h		move left 30

bindsym $mod+j			focus down
bindsym $mod+Shift+j		move down 30

bindsym $mod+k			focus up
bindsym $mod+Shift+k		move up 30

bindsym $mod+l			focus right
bindsym $mod+Shift+l		move right 30

bindsym $mod+z			gaps outer current plus 5
bindsym $mod+Shift+z		gaps outer current minus 5

bindsym $mod+x			exec --no-startup-id lockscreen
bindsym $mod+Shift+x		exec --no-startup-id prompt "Shutdown computer?" "$shutdown"

bindsym $mod+Shift+b		floating toggle; sticky toggle; exec --no-startup-id bottomleft

# #---Workspace Bindings---# #
bindsym $mod+Home		workspace $ws1
bindsym $mod+Shift+Home		move container to workspace $ws1
bindsym $mod+End		workspace $ws10
bindsym $mod+Shift+End		move container to workspace $ws10
bindsym $mod+Prior		workspace prev
bindsym $mod+Shift+Prior	move container to workspace prev
bindsym $mod+Next		workspace next
bindsym $mod+Shift+Next		move container to workspace next
bindsym $mod+Tab		workspace back_and_forth
bindsym $mod+XF86Back		workspace prev
bindsym $mod+Shift+XF86Back	move container to workspace prev
bindsym $mod+XF86Forward	workspace next
bindsym $mod+Shift+XF86Forward	move container to workspace next
bindsym $mod+semicolon		workspace next
bindsym $mod+slash		split vertical ;; exec $term
bindsym $mod+Shift+slash	kill
bindsym $mod+backslash		workspace back_and_forth

set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1		workspace $ws1
bindsym $mod+2		workspace $ws2
bindsym $mod+3		workspace $ws3
bindsym $mod+4		workspace $ws4
bindsym $mod+5		workspace $ws5
bindsym $mod+6		workspace $ws6
bindsym $mod+7		workspace $ws7
bindsym $mod+8		workspace $ws8
bindsym $mod+9		workspace $ws9
bindsym $mod+0		workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1	move container to workspace $ws1
bindsym $mod+Shift+2	move container to workspace $ws2
bindsym $mod+Shift+3	move container to workspace $ws3
bindsym $mod+Shift+4	move container to workspace $ws4
bindsym $mod+Shift+5	move container to workspace $ws5
bindsym $mod+Shift+6	move container to workspace $ws6
bindsym $mod+Shift+7	move container to workspace $ws7
bindsym $mod+Shift+8	move container to workspace $ws8
bindsym $mod+Shift+9	move container to workspace $ws9
bindsym $mod+Shift+0	move container to workspace $ws10

for_window [window_role="GtkFileChooserDialog"] resize shrink height 10 px

# #---Function Buttons---# #
bindsym $mod+F2		restart
bindsym $mod+F3		exec --no-startup-id displayselect
bindsym $mod+F4		exec --no-startup-id prompt "Hibernate computer?" "$hibernate"
bindsym $mod+F5		exec --no-startup-id $netrefresh
bindsym $mod+F9		exec --no-startup-id dmenumount
bindsym $mod+F10	exec --no-startup-id dmenuumount
bindsym $mod+F12	exec $term -e sudo -A wifi-menu

# #---Arrow Keys---# #
bindsym $mod+Left		focus left
bindsym $mod+Down		focus down
bindsym $mod+Up			focus up
bindsym $mod+Right 		focus right
bindsym $mod+Shift+Left		move left
bindsym $mod+Shift+Down		move down
bindsym $mod+Shift+Up		move up
bindsym $mod+Shift+Right 	move right

# #---Media Keys---# #

# For screenshots and recording
bindsym Print 			exec --no-startup-id scrot
bindsym Shift+Print 		exec --no-startup-id scrot -u

# #---Extra XF86 Keys---# #
# These are the extra media keys that some keyboards have.
bindsym XF86AudioMute		exec --no-startup-id pulsemixer --toggle-mute mute && pkill -RTMIN+10 i3blocks
bindsym $mod+m		exec --no-startup-id pulsemixer --toggle-mute && pkill -RTMIN+10 i3blocks
bindsym XF86AudioLowerVolume	exec --no-startup-id pulsemixer --change-volume -5 && pkill -RTMIN+10 i3blocks
bindsym Shift+XF86AudioLowerVolume	exec --no-startup-id pulsemixer --change-volume -10 && pkill -RTMIN+10 i3blocks 
bindsym Control+XF86AudioLowerVolume	exec --no-startup-id pulsemixer --change-volume -1 && pkill -RTMIN+10 i3blocks 
bindsym XF86AudioRaiseVolume	exec --no-startup-id pulsemixer --change-volume +5 && pkill -RTMIN+10 i3blocks
bindsym Shift+XF86AudioRaiseVolume	exec --no-startup-id pulsemixer --change-volume +10 && pkill -RTMIN+10 i3blocks
bindsym Control+XF86AudioRaiseVolume	exec --no-startup-id pulsemixer --change-volume +1 && pkill -RTMIN+10 i3blocks 
bindsym XF86PowerOff		exec --no-startup-id prompt "Shutdown computer?" "$shutdown"
bindsym XF86MonBrightnessDown	exec --no-startup-id sudo xbacklight -15
bindsym XF86MonBrightnessUp	exec --no-startup-id sudo xbacklight +15
